﻿using System;
using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public string VerificarCategoria(string nome)
        {
            string categoria = "";
            string[] palavras = nome.Split(' ');

            for (var i = 0; i < palavras.Length; i++)
            {
                if (palavras[i] == "Conjurado")
                {
                    categoria = palavras[i];
                
                    return categoria;
                }
            }
            return categoria;
        }

        public void AtualizarQualidade()
        {

            for (var i = 0; i < Itens.Count; i++)
            {
                if (Itens[i].Nome != "Sulfuras, a Mão de Ragnaros")
                {
                    Itens[i].PrazoValidade--;

                }

                //produtos no prazo

                if (Itens[i].PrazoValidade >= 0)
                {
                    switch (Itens[i].Nome)
                    {
                        case "Ingressos para o concerto do TAFKAL80ETC":
                            Itens[i].Qualidade++;

                            if (Itens[i].PrazoValidade <= 10)
                            {
                                Itens[i].Qualidade++;
                            }
                            if (Itens[i].PrazoValidade <= 5)
                            {
                                Itens[i].Qualidade++;
                            }
                            break;

                        case "Queijo Brie Envelhecido":
                            if (Itens[i].Qualidade < 50)
                            {
                                Itens[i].Qualidade++;
                            }
                            break;

                        default:
                            Itens[i].Qualidade--;
                            if (VerificarCategoria(Itens[i].Nome) == "Conjurado")
                            {
                                Itens[i].Qualidade--;
                            }
                            break;

                    }
                }

                //Prazo de validade expirado

                else
                {
                    switch (Itens[i].Nome)
                    {
                        case "Ingressos para o concerto do TAFKAL80ETC":
                            Itens[i].Qualidade = 0;
                            break;

                        case "Queijo Brie Envelhecido":
                            if (Itens[i].Qualidade < 50)
                            {
                                Itens[i].Qualidade += 2;
                            }
                            break;

                        default:
                            Itens[i].Qualidade -= 2;

                            if (VerificarCategoria(Itens[i].Nome) == "Conjurado")
                            {
                                Itens[i].Qualidade -= 2;
                            }
                            break;

                    }
                }

                if (Itens[i].Nome != "Sulfuras, a Mão de Ragnaros")
                {
                    Itens[i].Qualidade = Itens[i].Qualidade > 50 ? 50 : Itens[i].Qualidade;
                    Itens[i].Qualidade = Itens[i].Qualidade < 0 ? 0 : Itens[i].Qualidade;
                }
                else
                {
                    Itens[i].Qualidade = 80;
                }

            }

        }
    }
}
