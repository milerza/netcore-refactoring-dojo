﻿using System;
using System.Collections.Generic;


namespace MeuAcerto.Selecao.KataGildedRose
{
    class Program
    {
        public static void Main(string[] args)
        {
            IList<Item> itens = ReadData.CsvItemsReader();
            if (itens != null)
            {
                var app = new GildedRose(itens);

                for (var i = 0; i < 31; i++)
                {
                    Console.WriteLine("-------- dia " + i + " --------");
                    Console.WriteLine("Nome, PrazoValidade, Qualidade");
                    for (var j = 0; j < itens.Count; j++)
                    {
                        Console.WriteLine(itens[j].Nome + ", " + itens[j].PrazoValidade + ", " + itens[j].Qualidade);
                    }
                    Console.WriteLine("");
                    app.AtualizarQualidade();
                }
            }
            else
            {
                Console.WriteLine("Não há dados para serem lidos!");
            }

        }

    }
}
