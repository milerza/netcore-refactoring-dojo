﻿using CsvHelper.Configuration;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class ItemMap : ClassMap<Item>
    {
        public ItemMap()
        {
            Map(m => m.Nome).Name("Nome");
            Map(m => m.PrazoValidade).Name("PrazoValidade");
            Map(m => m.Qualidade).Name("Qualidade");
        }
    }

}

