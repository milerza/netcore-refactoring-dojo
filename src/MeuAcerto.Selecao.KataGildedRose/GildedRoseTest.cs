﻿using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseTest
    {
        [Fact]
        public void Foo(){
            IList<Item> Items = new List<Item> { new Item { Nome = "foo", PrazoValidade = 0, Qualidade = 0 } };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal("foo", Items[0].Nome);
        }

        [Fact]
        public void WhenTheItemIsConjuradoButTheQualityIsZeroThenTheItemIsNotDecremented()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Bolo de Mana Conjurado", PrazoValidade = 10, Qualidade = 0 },
                                                 new Item { Nome = "Bolo de Mana Conjurado", PrazoValidade = -1, Qualidade = 0},
                                                 new Item { Nome = "Bolo de Mana Conjurado", PrazoValidade = 0, Qualidade = 0} };
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(0, Items[0].Qualidade);
            Assert.Equal(0, Items[1].Qualidade);
            Assert.Equal(0, Items[2].Qualidade);
        }

        [Fact]
        public void WhenTheItemIsConjuradoButTheQualityIsMoreThanZeroThenTheItemIsDecremented()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Bolo de Mana Conjurado", PrazoValidade = 10, Qualidade = 10 },
                                                 new Item { Nome = "Bolo de Mana Conjurado", PrazoValidade = -1, Qualidade = 10 },
                                                 new Item { Nome = "Bolo de Mana Conjurado", PrazoValidade = 0, Qualidade = 10 },
                                                 new Item { Nome = "Bolo de Mana Conjurado", PrazoValidade = -1, Qualidade = 3 }};
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(8, Items[0].Qualidade);
            Assert.Equal(6, Items[1].Qualidade);
            Assert.Equal(6, Items[2].Qualidade);
            Assert.Equal(0, Items[3].Qualidade);
        }

        [Fact]
        public void WhenTheItemIngressosParaOConcertoDoTAFKAL80ETCButTheQualityIsLessThanFiftyThenTheItemIsIncremented()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC", PrazoValidade = 5, Qualidade = 10 },
                                                 new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC", PrazoValidade = 10, Qualidade = 10 },
                                                 new Item { Nome ="Ingressos para o concerto do TAFKAL80ETC", PrazoValidade = 20, Qualidade = 10 },
                                                 new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC", PrazoValidade = -1, Qualidade = 10 },
                                                 new Item { Nome = "Ingressos para o concerto do TAFKAL80ETC", PrazoValidade = 5, Qualidade = 50 }};
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(13, Items[0].Qualidade);
            Assert.Equal(12, Items[1].Qualidade);
            Assert.Equal(11, Items[2].Qualidade);
            Assert.Equal(0, Items[3].Qualidade);
            Assert.Equal(50, Items[4].Qualidade);
        }
        [Fact]
        public void WhenTheItemIsQueijoBrieEnvelhecidoButTheQualityIsLessThanFiftyThenTheItemIsIncremented()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Queijo Brie Envelhecido", PrazoValidade = 1, Qualidade = 10 },
                                                 new Item { Nome = "Queijo Brie Envelhecido", PrazoValidade = 0, Qualidade = 49 },
                                                 new Item { Nome = "Queijo Brie Envelhecido", PrazoValidade = -1, Qualidade = 10 },
                                                 new Item { Nome = "Queijo Brie Envelhecido", PrazoValidade = 0, Qualidade = 10 }};

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(11, Items[0].Qualidade);
            Assert.Equal(50, Items[1].Qualidade);
            Assert.Equal(12, Items[2].Qualidade);
            Assert.Equal(12, Items[3].Qualidade);

        }
        [Fact]
        public void WhenTheItemIsSulfurasAMaoDeRagnarosThenTheQualityIsAlweysEighty()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = 1, Qualidade = 10 },
                                                 new Item { Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = 0, Qualidade = 49 },
                                                 new Item { Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = -1, Qualidade = 10 }, };
            
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(80, Items[0].Qualidade);
            Assert.Equal(80, Items[1].Qualidade);
            Assert.Equal(80, Items[2].Qualidade);
        }
        [Fact]
        public void WhenTheItemIsNormalThenTheQualityIsDecremented()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Corselete +5 DEX", PrazoValidade = 1, Qualidade = 10 },
                                                 new Item { Nome = "Corselete +5 DEX", PrazoValidade = 0, Qualidade = 10 },
                                                 new Item { Nome = "Corselete +5 DEX", PrazoValidade = -1, Qualidade = 10 }, };

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();
            Assert.Equal(9, Items[0].Qualidade);
            Assert.Equal(8, Items[1].Qualidade);
            Assert.Equal(8, Items[2].Qualidade);
        }
    }
}
