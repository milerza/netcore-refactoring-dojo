﻿using CsvHelper;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class ReadData
    {
        public static List<Item> CsvItemsReader()
        {
            string path;

            if (File.Exists(Path.GetFullPath("../Items.csv")))
            {
                path = Path.GetFullPath("../Items.csv");
            }
            else if (File.Exists(Path.GetFullPath("../../../Items.csv")))
            {
                path = Path.GetFullPath("../../../Items.csv");
            }
            else
            {
                return null;
            }

            using var reader = new StreamReader(path);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);

            csv.Context.RegisterClassMap<ItemMap>();
            var records = csv.GetRecords<Item>().ToList();

            return records;

        }

    }
}