##Objetivo:
 Vender uma nova categoria de itens

##Descrição:    
 alterar método AtualizarQualidade para adicionar os itens "Conjurados" (`Conjurado`) diminuem a (`Qualidade`) duas vezes mais rápido que os outros itens.

##TO DO
    * Verificar se um item é conjurado. Itens conjurados possuem a palavra `Conjurado` no nome.
        Criar metodo VerificarCategoria(String Nome) para verificar a categoria Conjurado
         -> posteriormente refatorar o metodo para verificar outras categorias;
         -> posteriormente refatorar o método para verificar se os itens possuem mais de uma categoria;

    * Modificar o método Atualizar qualidade para decrementar os itens "Conjurados" 2x mais rápido que o resto.
    ->refatorar o código para verificar inicialmente a data de validade.
    -> modularizar mais o codigo